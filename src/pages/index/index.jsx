import React, { useState } from 'react'
import Taro from '@tarojs/taro'
import VirtualList from '@tarojs/components/virtual-list'
import { Image, View, Text } from '@tarojs/components'
import './index.scss'


import Pic1 from '../../images/1.png'
import Pic2 from '../../images/2.png'
import Pic3 from '../../images/3.png'
import Pic4 from '../../images/4.png'
import Pic5 from '../../images/5.png'




const photos = [
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic1, time:null},
  {src: Pic2, time:null},
  {src: Pic3, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
  {src: Pic4, time:null},
  {src: Pic5, time:null},
]




const Row = React.memo(({ index, style, data }) => {

  return (
    <View style={style} className='rowPhotos'>
      {/* <Text>{index}</Text> */}
      <Image mode='aspectFill' className='photos' src={data[index*3].src}></Image>
      <Image mode='aspectFill' className='photos' src={data[index*3+1].src}></Image>

      <Image mode='aspectFill' className='photos' src={data[index*3+2].src}></Image>


    </View>
  )
})

export default function Index() {

  const res = Taro.getSystemInfoSync()
  const itemSize = res.screenWidth * 0.32
  const height = res.screenHeight * 0.82 - 20

  const [data, setData] = useState(photos)
  const dataLen = Math.ceil(data.length / 3)

    return (
      <VirtualList
        className='List'
        height={height}
        itemData={data}
        itemCount={dataLen}
        itemSize={itemSize}
        // useIsScrolling='true'
        overscanCount={20}
        width='100%'
      >
        {Row}
      </VirtualList>
    )
  
}
